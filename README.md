# Spacious dokuwiki template

* Based on a wordpress theme
* Designed by [Themegrill](https://wordpress.org/themes/spacious/)
* Converted by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:spacious)

![spacious theme screenshot](https://i.imgur.com/RnXdrRY.png)