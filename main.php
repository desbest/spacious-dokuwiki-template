<?php
/**
 * DokuWiki Spacious Template
 * Based on the starter template and a wordpress theme of the same name
 *
 * @link     http://dokuwiki.org/template:spacious
 * @author   desbest <afaninthehouse@gmail.com>
 * @license  GPL 2 (http://www.gnu.org/licenses/gpl.html)
 */

if (!defined('DOKU_INC')) die(); /* must be run from within DokuWiki */
@require_once(dirname(__FILE__).'/tpl_functions.php'); /* include hook for template functions */
header('X-UA-Compatible: IE=edge,chrome=1');

$showTools = !tpl_getConf('hideTools') || ( tpl_getConf('hideTools') && !empty($_SERVER['REMOTE_USER']) );
$showSidebar = page_findnearest($conf['sidebar']) && ($ACT=='show');
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $conf['lang'] ?>"
  lang="<?php echo $conf['lang'] ?>" dir="<?php echo $lang['direction'] ?>" class="no-js">
<head>
    <meta charset="UTF-8" />
    <title><?php tpl_pagetitle() ?> [<?php echo strip_tags($conf['title']) ?>]</title>
    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
    <?php tpl_metaheaders() ?>
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <?php echo tpl_favicon(array('favicon', 'mobile')) ?>
    <?php tpl_includeFile('meta.html') ?>
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet"> 
<script defer type="text/javascript" src="<?php echo tpl_basedir();?>/mobileinputsize.js"></script>

</head>

<body>

<div id="page" class="hfeed site">
<a class="skip-link screen-reader-text" href="#main"></a>

<header id="masthead" class="site-header clearfix">

    
<div id="header-meta">
    <div class="inner-wrap clearfix">
    <nav class="small-menu" class="clearfix">
    </nav>
    </div>
</div>


<div id="header-text-nav-container" class="">

    <div class="inner-wrap" id="">

        <div id="header-text-nav-wrap" class="clearfix">
            <div id="header-left-section">
                <div id="header-logo-image">

                </div><!-- #header-logo-image -->

                <div id="header-text" class="">
                        <h1 id="site-title"  rel="home" title=""><?php tpl_link(wl(),$conf['title'],'accesskey="h" title="[H]"') ?></h1>
                        <h3 id="site-title">
                            <a href=""
                               title=""
                               rel="home"></a>
                        </h3>
                   
                    <p id="site-description"></p>
                    <!-- #site-description -->
                </div><!-- #header-text -->
            </div><!-- #header-left-section -->
            <div id="header-right-section">
             
                <div id="header-right-sidebar" class="clearfix">
                    <?php if ($conf['useacl'] && $showTools): ?>
                    <!-- <div id="dokuwiki__usertools"> -->
                        <h3 class="a11y"><?php echo $lang['user_tools'] ?></h3>

          <nav id="site-navigation" class="main-navigation clearfix   " role="navigation">
            <p class="menu-toggle">Menu</p>
            <div class="menu-primary-container">
            <ul id="menu-my-first-menu" class="menu nav-menu">
              <?php /* the optional second parameter of tpl_action() switches between a link and a button,
                             e.g. a button inside a <li> would be: tpl_action('edit', 0, 'li') */
                ?>
                <?php tpl_toolsevent('usertools', array(
                    'admin'     => tpl_action('admin', 1, 'li', 1),
                    'userpage'  => _tpl_action('userpage', 1, 'li', 1),
                    'profile'   => tpl_action('profile', 1, 'li', 1),
                    'register'  => tpl_action('register', 1, 'li', 1),
                    'login'     => tpl_action('login', 1, 'li', 1),
                )); ?>
                </ul>
                <ul>
                <?php tpl_toolsevent('sitetools', array(
                    'recent'    => tpl_action('recent', 1, 'li', 1),
                    'media'     => tpl_action('media', 1, 'li', 1),
                    'index'     => tpl_action('index', 1, 'li', 1),
                )); ?>
            </ul>
            </div>     
            </nav>
                      
                    <!-- </div> -->
                <?php endif ?>
                </div> <!-- header right sidebar -->
                       
                <div class="header-action">

          
                  
                        <div class="search-wrapper">
                            <div class="search">
                                <i class="fa fa-search"> </i>
                            </div>
                            <div class="header-search-form">
                                
                            </div>
                        </div><!-- /.search-wrapper -->
                </div>
            </div><!-- #header-right-section -->

        </div><!-- #header-text-nav-wrap -->
    </div><!-- .inner-wrap -->
    <div class="bottom-menu clearfix">
        <div class="inner-wrap clearfix"> <!-- this div is big -->



            <div class="header-action">

                    <div class="search-wrapper">
                        
                        <div class="search">
                            <i class="fa fa-search"> </i>
                        </div>
                        <div class="header-search-form">

                        </div>
                    </div><!-- /.search-wrapper -->
            </div>
        </div>
    </div>
</div><!-- #header-text-nav-container -->

    <div class="header-post-title-container clearfix">
        <div class="inner-wrap">
            <div class="post-title-wrapper">
                <!-- BREADCRUMBS -->
                <?php if($conf['breadcrumbs']){ ?>
                    <div class="breadcrumbs"><?php tpl_breadcrumbs() ?></div>
                <?php } ?>
                <?php if($conf['youarehere']){ ?>
                    <div class="breadcrumbs"><?php tpl_youarehere() ?></div>
                <?php } ?>
                                        <h2 class="header-post-title-class"></h2>
                        <h1 class="header-post-title-class"></h1>
            </div>
        </div>
    </div>
</header>
<!-- /////////////-- end of header --//////////// -->

    <div id="main" class="clearfix">
        <div class="inner-wrap">

            <!-- end spacious header -->


    <?php /* with these Conditional Comments you can better address IE issues in CSS files,
             precede CSS rules by #IE8 for IE8 (div closes at the bottom) */ ?>
    <!--[if lte IE 8 ]><div id="IE8"><![endif]-->

    <?php /* the "dokuwiki__top" id is needed somewhere at the top, because that's where the "back to top" button/link links to */ ?>
    <?php /* tpl_classes() provides useful CSS classes; if you choose not to use it, the 'dokuwiki' class at least
             should always be in one of the surrounding elements (e.g. plugins and templates depend on it) */ ?>
    <div id="dokuwiki__site"><div id="dokuwiki__top" class="site <?php echo tpl_classes(); ?> <?php
        echo ($showSidebar) ? 'hasSidebar' : ''; ?>">
        <?php html_msgarea() /* occasional error and info messages on top of the page */ ?>
        <?php tpl_includeFile('header.html') ?>

        <!-- ********** HEADER ********** -->
        <div id="dokuwiki__header"><div class="pad">

            <div class="headings">
                <?php /* how to insert logo instead (if no CSS image replacement technique is used):
                        upload your logo into the data/media folder (root of the media manager) and replace 'logo.png' accordingly:
                        tpl_link(wl(),'<img src="'.ml('logo.png').'" alt="'.$conf['title'].'" />','id="dokuwiki__top" accesskey="h" title="[H]"') */ ?>
                <?php if ($conf['tagline']): ?>
                    <p class="claim"><?php echo $conf['tagline'] ?></p>
                <?php endif ?>

                <ul class="a11y skip">
                    <li><a href="#dokuwiki__content"><?php echo $lang['skip_to_content'] ?></a></li>
                </ul>
                <div class="clearer"></div>
            </div>

            
            <div class="clearer"></div>

            

            <div class="clearer"></div>
            <hr class="a11y" />
        </div></div><!-- /header -->


        <div class="wrapper">

            <!-- ********** CONTENT ********** -->
            <div id="dokuwiki__content"><!-- <div class="pad"> -->
                <?php tpl_flush() /* flush the output buffer */ ?>
                <?php tpl_includeFile('pageheader.html') ?>



                <div class="page">

                    <div id="primary">
                        <div id="content" class="clearfix">
                        <!-- wikipage start -->
                        <?php tpl_content() /* the main content */ ?>
                        <!-- wikipage stop -->
                        </div>
                    </div>
                    
                    <div id="secondary">
                        <aside id="search" class="widget widget_search">
                            <?php tpl_searchform() ?>
                        </aside>

                        <aside id="archives" class="widget">
                            <h3 class="widget-title">Page Actions</h3>
                            <!-- PAGE ACTIONS -->
                            <?php if ($showTools): ?>
                                    <h3 class="a11y"><?php echo $lang['page_tools'] ?></h3>
                                    <ul>
                                        <?php tpl_toolsevent('pagetools', array(
                                            'edit'      => tpl_action('edit', 1, 'li', 1),
                                            'discussion'=> _tpl_action('discussion', 1, 'li', 1),
                                            'revisions' => tpl_action('revisions', 1, 'li', 1),
                                            'backlink'  => tpl_action('backlink', 1, 'li', 1),
                                            'subscribe' => tpl_action('subscribe', 1, 'li', 1),
                                            'revert'    => tpl_action('revert', 1, 'li', 1),
                                            'top'       => tpl_action('top', 1, 'li', 1),
                                        )); ?>
                                    </ul>
                            <?php endif; ?>
                        </aside>

                        <?php if ($showSidebar): ?>
                        <aside id="writtensidebar" class="widget">
                            <!-- <h3 class="widget-title">Sidebar</h3> -->
                            <?php tpl_includeFile('sidebarheader.html') ?>
                            <?php tpl_include_page($conf['sidebar'], 1, 1) /* includes the nearest sidebar page */ ?>
                            <?php tpl_includeFile('sidebarfooter.html') ?>
                            <div class="clearer"></div>
                        </aside>
                        <?php endif; ?>     
                    </div> 
                        <!-- close secondary -->
                    <!-- </div>  -->
                    <!-- close dokuwiki__content -->
                     


                

                    <div class="clearer"></div>
                </div>

                <?php tpl_flush() ?>
                <?php tpl_includeFile('pagefooter.html') ?>
            <!-- </div> --></div><!-- /content -->

            <div class="clearer"></div>
            <hr class="a11y" />


            <!-- ********** ASIDE ********** -->
                
            
        </div><!-- /wrapper -->

        <!-- ********** FOOTER ********** -->
        

        <?php tpl_includeFile('footer.html') ?>
    </div></div><!-- /site -->

    <div class="no"><?php tpl_indexerWebBug() /* provide DokuWiki housekeeping, required in all templates */ ?></div>
    <!--[if lte IE 8 ]></div><![endif]-->

        </div><!-- .inner-wrap -->
</div><!-- #main -->


<footer id="colophon" class="clearfix">
    <div class="footer-socket-wrapper clearfix">
        <div class="inner-wrap">
            <div class="footer-socket-area">
                <div id="dokuwiki__footer"><div class="pad">
                    <?php
                                            if (!empty($_SERVER['REMOTE_USER'])) {
                                                echo '';
                                                tpl_userinfo(); /* 'Logged in as ...' */
                                                echo '<br>';
                                            }
                                        ?>
            <div class="doc"><?php tpl_pageinfo() /* 'Last modified' etc */ ?></div>
            <?php tpl_license('button') /* content license, parameters: img=*badge|button|0, imgonly=*0|1, return=*0|1 */ ?>
        </div></div><!-- /footer -->
                <nav class="small-menu clearfix">
                    
                </nav>
            </div>
        </div>
    </div>
</footer>
<a href="#masthead" id="scroll-up"></a>
</div><!-- #page -->
 
    <!-- due to the way dokuwiki buffers output, this javascript has to
            be before the </body> tag and not in the <head> -->
    <script defer type="text/javascript" src="<?php echo tpl_basedir();?>/navigation.js"></script>
</body>
</html>
